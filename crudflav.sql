drop schema if exists games_crud;
CREATE DATABASE games_crud;

USE games_crud;

CREATE TABLE Games (
  ID_game int NOT NULL AUTO_INCREMENT primary key,
  nome varchar(60) NOT NULL,
  desenvolvedora varchar(50) NOT NULL,
  preco decimal(10,2) NOT NULL,
  plataforma varchar(30) NOT NULL
);

INSERT INTO Games (ID_game, nome, desenvolvedora,preco,plataforma) VALUES 
(1,"Mario", "Nintendo", 80.0, "Nintend Switch");

INSERT INTO Games (ID_game, nome, desenvolvedora,preco,plataforma) VALUES 
(2,"Far Cry 5", "Ubisoft", 239.90, "PC");



UPDATE Games SET nome = "Far Cry 4", desenvolvedora = "Ubisoft" , preco = 3 where ID_game = 3;

create table financ(
	id_valor int NOT NULL AUTO_INCREMENT primary key ,
	valor decimal(8,2)
);

select * from financ;

insert into financ values
(1,0);

UPDATE financ SET valor = 100
where id_valor = 1;

select * from financ








