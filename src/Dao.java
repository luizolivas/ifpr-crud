/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.util.List;

/**
 *
 * @author ALUNO
 */
public interface Dao {
    
    void adicionar(Game game);
    void atualizar(Game game);
    void remover(Game game);
    Game buscapreco();
    void addPreco(Game game);
    Game buscaPorID(int id);
    List<Game> buscaTodos();
}
