
package crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ALUNO
 */
public class GameDao implements Dao {

    private Connection conexao;
    private PreparedStatement operacao;
    private ResultSet result;

    public GameDao() {
        conexao = new BuilderConn().getConnection();
    }

    @Override
    public void adicionar(Game game) {
        String sql = "INSERT INTO Games " + "(nome, desenvolvedora, preco, plataforma) "
                + "VALUES (?, ?, ?, ?)";
        try {
            operacao = conexao.prepareStatement(sql);
            operacao.setString(1, game.getNome());
            operacao.setString(2, game.getDesenvolvedora());
            operacao.setDouble(3, game.getPreco());
            operacao.setString(4, game.getPlataforma());
            operacao.execute();
        } catch (SQLException ex) {
            Logger.getLogger(GameDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        @Override
    public void remover(Game game) {
        String sql = "DELETE FROM Games WHERE ID_game = ?";
        try {
            operacao = conexao.prepareStatement(sql);
            operacao.setInt(1, game.getId());
            operacao.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(GameDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void atualizar(Game game) {
        String sql = "UPDATE Games SET nome = ?, desenvolvedora = ? , preco = ?, plataforma = ? where ID_game = ?";
        try {
            operacao = conexao.prepareStatement(sql);
            operacao.setString(1, game.getNome());
            operacao.setString(2, game.getDesenvolvedora());
            operacao.setDouble(3, game.getPreco());
            operacao.setString(4, game.getPlataforma());
            operacao.setInt(5, game.getId());
            operacao.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(GameDao.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    @Override
    public Game buscaPorID(int id) {
        String sql = "SELECT * FROM games WHERE ID_game = ?";
        try {
            operacao = conexao.prepareStatement(sql);
            operacao.setInt(1, id);
            result = operacao.executeQuery();
            result.next();
            Game g = new Game();
            g.setId(result.getInt("ID_game"));
            g.setNome(result.getString("nome"));
            g.setDesenvolvedora(result.getString("desenvolvedora"));
            g.setPreco(result.getDouble("preco"));
            g.setPlataforma(result.getString("plataforma"));
            g.setPreco(0);
            return g;
        } catch (SQLException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<Game> buscaTodos() {
        String sql = "SELECT * FROM games";
        List<Game> list = new ArrayList<>();
        try {

            operacao = conexao.prepareStatement(sql);
            result = operacao.executeQuery();
            while (result.next()) {
                Game g = new Game();
                g.setId(result.getInt("ID_game"));
                g.setNome(result.getString("nome"));
                g.setDesenvolvedora(result.getString("desenvolvedora"));
                g.setPreco(result.getDouble("preco"));
                g.setPlataforma(result.getString("plataforma"));
                list.add(g);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(GameDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Game buscapreco() {
        String sql = "SELECT * FROM financ";

        try {
            operacao = conexao.prepareStatement(sql);
            result = operacao.executeQuery();
            result.next();
            Game g = new Game();
            g.setLucro(result.getDouble("valor"));
            return g;
            
        } catch (SQLException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void addPreco(Game game) {
        String sql = "UPDATE financ SET valor = ? where id_valor = 1";
        try {
            operacao = conexao.prepareStatement(sql);
            operacao.setDouble(1, game.getLucro());
            operacao.executeUpdate();
        }
        catch (SQLException ex) {
            //Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}

