/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

/**
 *
 * @author ALUNO
 */
public class Game {
    
    private int id;
    private String nome;
    private String desenvolvedora;
    private double preco;
    private String plataforma;
    private double lucro;
    
    public Game(){
        
    }

    public Game(int id, String nome, String desenvolvedora, double preco, String plataforma,double lucro) {
        this.id = id;
        this.nome = nome;
        this.desenvolvedora = desenvolvedora;
        this.preco = preco;
        this.plataforma = plataforma;
        this.lucro = lucro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesenvolvedora() {
        return desenvolvedora;
    }

    public void setDesenvolvedora(String desenvolvedora) {
        this.desenvolvedora = desenvolvedora;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public double getLucro() {
        return lucro;
    }

    public void setLucro(double lucro) {
        this.lucro = lucro;
    }


    
    

    @Override
    public String toString() {
        return "Game{" + "id=" + id + ", nome=" + nome + ", desenvolvedora=" + desenvolvedora + ", preco=" + preco + '}';
    }


    
}
