/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ALUNO
 */
public class BuilderConn {
    
  
    private String url = "jdbc:mysql://localhost/games_crud";
    private String user = "";
    private String password = "";
    
    public Connection getConnection(){
        try {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(BuilderConn.class.getName()).log(Level.SEVERE, null, ex);
            }
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException ex) {
            Logger.getLogger(BuilderConn.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}

